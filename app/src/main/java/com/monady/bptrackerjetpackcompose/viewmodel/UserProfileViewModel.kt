package com.monady.bptrackerjetpackcompose.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.monady.bptrackerjetpackcompose.models.UserProfile
import com.monady.bptrackerjetpackcompose.repository.DataRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserProfileViewModel @Inject constructor(private val repository: DataRepository) : ViewModel() {

    val userProfile = repository.getUserProfile()

    private val _initialProfile = MutableLiveData<UserProfile>()
    val initialProfile: LiveData<UserProfile>
        get() = _initialProfile

    suspend fun addUserProfile(
        userProfile: UserProfile
    ){
        viewModelScope.launch {
            val timestamp = System.currentTimeMillis()
            repository.insertUserProfile(
                userProfile = UserProfile(
                    created_at = timestamp,
                    updated_at = timestamp,
                    firstName = userProfile.firstName,
                    lastName = userProfile.lastName,
                    bloodType = userProfile.bloodType,
                    normalHighReading = userProfile.normalHighReading,
                    normalLowReading = userProfile.normalLowReading,
                    emergencyContactName = userProfile.emergencyContactName,
                    emergencyContactNumber = userProfile.emergencyContactNumber
                )
            )
        }
    }

    suspend fun updateUserProfile(userProfile: UserProfile){
        viewModelScope.launch {
            userProfile.updated_at = System.currentTimeMillis()
            repository.updateUserProfile(userProfile)
        }
    }

    fun setFirstName(firstName: String){
        _initialProfile.value?.let{
            it.firstName = firstName
        }
    }

    fun setLastName(lastName: String){
        _initialProfile.value?.let{
            it.lastName = lastName
        }
    }

    fun setBloodType(bloodType: String){
        _initialProfile.value?.let{
            it.bloodType = bloodType
        }
    }

    fun setNormalHighReading(highReading: Int){
        _initialProfile.value?.let {
            it.normalHighReading = highReading
        }
    }

    fun setNormalLowReading(lowReading: Int){
        _initialProfile.value?.let {
            it.normalLowReading = lowReading
        }
    }

    fun setEmergencyContactName(contactName: String){
        _initialProfile.value?.let {
            it.emergencyContactName = contactName
        }
    }

    fun setEmergencyContactNumber(contactNumber: String){
        _initialProfile.value?.let {
            it.emergencyContactNumber = contactNumber
        }
    }
}