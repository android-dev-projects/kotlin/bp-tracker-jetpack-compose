package com.monady.bptrackerjetpackcompose.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.monady.bptrackerjetpackcompose.models.BpReading
import com.monady.bptrackerjetpackcompose.repository.DataRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BpViewModel @Inject constructor(private val repository: DataRepository) : ViewModel() {

    val bpReadingsList = repository.getAllReadings()

    private val _reading = MutableLiveData<BpReading>()
    val reading: LiveData<BpReading>
        get() = _reading

    fun updateHighReading(highReading: Int){
        _reading.value?.let {
            it.high = highReading
        }
    }

    fun updateLowReading(lowReading: Int){
        _reading.value?.let {
            it.low = lowReading
        }
    }

    suspend fun addReading(high: Int, low: Int){
        viewModelScope.launch {
            val timestamp = System.currentTimeMillis()
            repository.insertReading(
                reading = BpReading(
                    created_at = timestamp,
                    updated_at = timestamp,
                    high = high,
                    low = low
                )
            )
        }
    }

    suspend fun updateReading(reading: BpReading){
        viewModelScope.launch {
            reading.updated_at = System.currentTimeMillis()
            repository.updateReading(reading)
        }
    }

    suspend fun deleteReadingById(readingId: Int){
        viewModelScope.launch {
            repository.deleteReadingById(readingId)
        }
    }

    suspend fun deleteAllReadings(){
        viewModelScope.launch {
            repository.deleteAllReadings()
        }
    }

    suspend fun getReadingById(readingId: Int){
        viewModelScope.launch {
            _reading.postValue(repository.getReadingById(readingId))
        }
    }
}