package com.monady.bptrackerjetpackcompose.di

import android.content.Context
import androidx.room.Room
import com.monady.bptrackerjetpackcompose.database.TrackerDatabase
import com.monady.bptrackerjetpackcompose.database.dao.BpReadingDao
import com.monady.bptrackerjetpackcompose.database.dao.UserProfileDao
import com.monady.bptrackerjetpackcompose.repository.DataRepository
import com.monady.bptrackerjetpackcompose.viewmodel.BpViewModel
import com.monady.bptrackerjetpackcompose.viewmodel.UserProfileViewModel
import dagger.Module
import dagger.Lazy
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun providesTrackerDatabase(@ApplicationContext context: Context) : TrackerDatabase = Room
        .databaseBuilder(
            context,
            TrackerDatabase::class.java,
            "tracker_database"
        )
        .build()

    @Provides
    @Singleton
    fun providesBpReadingsDao(trackerDatabase: TrackerDatabase): BpReadingDao = trackerDatabase.bpReadingDao

    @Provides
    @Singleton
    fun providesUserProfileDao(trackerDatabase: TrackerDatabase): UserProfileDao = trackerDatabase.userProfileDao

    @Provides
    @Singleton
    fun providesDataRepository(bpReadingDao: Lazy<BpReadingDao>, userProfileDao: Lazy<UserProfileDao>): DataRepository{
        return DataRepository(bpReadingDao, userProfileDao)
    }

    @Provides
    @Singleton
    fun providesBpViewModel(repository: DataRepository):BpViewModel{
        return BpViewModel(repository)
    }

    @Provides
    @Singleton
    fun providesUserProfileViewModel(repository: DataRepository): UserProfileViewModel{
        return UserProfileViewModel(repository)
    }
}