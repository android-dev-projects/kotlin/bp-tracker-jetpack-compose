package com.monady.bptrackerjetpackcompose.di

import android.content.Context
import com.monady.bptrackerjetpackcompose.utils.NotificationsUtil
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NotificationsModule {

    @Provides
    @Singleton
    fun providesNotificationsManager(@ApplicationContext context: Context): NotificationsUtil = NotificationsUtil(context)
}