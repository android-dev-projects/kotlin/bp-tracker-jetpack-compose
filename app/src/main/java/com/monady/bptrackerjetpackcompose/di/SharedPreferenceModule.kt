package com.monady.bptrackerjetpackcompose.di

import android.content.Context
import android.content.SharedPreferences
import com.monady.bptrackerjetpackcompose.utils.InitialPreferenceAppSetup
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object SharedPreferenceModule {

    @Provides
    @Singleton
    fun providesSharedPreferenceInstance(@ApplicationContext context: Context) : SharedPreferences =
        context.getSharedPreferences("BP_Tracker_Settings_JC",
        Context.MODE_PRIVATE
    )

    @Provides
    @Singleton
    fun providesAppSetup(sharedPreferences: SharedPreferences): InitialPreferenceAppSetup = InitialPreferenceAppSetup(sharedPreferences)
}