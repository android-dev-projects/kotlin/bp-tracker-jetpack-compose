package com.monady.bptrackerjetpackcompose.utils

import android.content.SharedPreferences
import javax.inject.Inject


class InitialPreferenceAppSetup @Inject constructor(private val sharedPreferences: SharedPreferences){

    private val firstTimeUse = "first_time_use"

    fun initFirstTimeUse(){
        if(!sharedPreferences.contains(firstTimeUse)){

            val editor = sharedPreferences.edit()

            editor.putBoolean(firstTimeUse, true)

            editor.apply()
        }
    }

    fun checkFirstTimeApp() = sharedPreferences.getBoolean(firstTimeUse, false)

    fun setFirstTimeAppEnd() = sharedPreferences.edit().putBoolean(firstTimeUse, false).apply()
}