package com.monady.bptrackerjetpackcompose.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.monady.bptrackerjetpackcompose.R
import javax.inject.Inject

class NotificationsUtil @Inject constructor(val context: Context) {

    companion object {
        private val CHANNEL_ID = "BPTCHD001"
        private var NOTIFICATION_ID = 0
    }

    fun createNotificationChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "BpTracker Notifications Channel"
            val description = "Welcome Message for the New Users"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            channel.description = description
            val notificationManager = context.getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel);
        }
    }

    fun buildNotification(title: String, content: String ){
        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_heart_logo)
            .setContentTitle(title)
            .setContentText(content)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        val notificationManager = NotificationManagerCompat.from(context)
        notificationManager.notify(NOTIFICATION_ID++, builder.build())
    }
}