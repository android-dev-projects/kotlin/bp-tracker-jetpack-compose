package com.monady.bptrackerjetpackcompose.navigation

enum class AppRoutes {
    Splash,
    FirstTimeProfile,
    ReadingDetails,
    NewReading,
    ReadingsList
}