package com.monady.bptrackerjetpackcompose.navigation

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.runtime.Composable
import androidx.compose.ui.focus.FocusManager
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.monady.bptrackerjetpackcompose.ui.screens.*
import com.monady.bptrackerjetpackcompose.viewmodel.BpViewModel
import com.monady.bptrackerjetpackcompose.viewmodel.UserProfileViewModel

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun SetNavHost(
    checkFirstTimeUse: Boolean,
    navHostController: NavHostController,
    focusManager: FocusManager,
    bpViewModel: BpViewModel,
    userViewModel: UserProfileViewModel,
    onExitCallback: () -> Unit = {},
    setFirstTimeAppEnd: () -> Unit = {}
){
    NavHost(
        navController = navHostController,
        startDestination = if(checkFirstTimeUse) AppRoutes.FirstTimeProfile.name else AppRoutes.ReadingsList.name
    ){
        composable(
            route = AppRoutes.Splash.name
        ){
            SplashScreen(navHostController = navHostController)
        }

        composable(
            route =  AppRoutes.FirstTimeProfile.name
        ){
            FirstTimeUserProfileScreen(
                focusManager = focusManager,
                viewModel = userViewModel,
                onSuccssfulUserCreation = {
                    navHostController.navigate(AppRoutes.ReadingsList.name){
                        popUpTo(0)
                    }
                    setFirstTimeAppEnd()
                },
                onExitCallback = onExitCallback
            )
        }

        composable(
            route = AppRoutes.ReadingDetails.name
        ){
            ReadingDetailsScreen(
                focusManager = focusManager,
                onGoBackClick = {
                    navHostController.popBackStack()
                },
                viewModel = bpViewModel
            )
        }

        composable(
            route = AppRoutes.NewReading.name
        ){
            NewReadingScreen(
                focusManager = focusManager,
                onGoBackClicked = {
                    navHostController.popBackStack()
                },
                viewModel = bpViewModel
            )
        }

        composable(
            route = AppRoutes.ReadingsList.name
        ){
            ReadingsListScreen(
                viewModel = bpViewModel,
                navHostController = navHostController,
            )
        }
    }
}