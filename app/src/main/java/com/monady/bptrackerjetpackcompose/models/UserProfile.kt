package com.monady.bptrackerjetpackcompose.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_profile_table")
data class UserProfile(
        @PrimaryKey(autoGenerate = true)
        val id: Int = 0,
        var created_at: Long = 0,
        var updated_at: Long = 0,
        var firstName: String = "",
        var lastName: String = "",
        var bloodType: String = "",
        var normalHighReading: Int = 0,
        var normalLowReading: Int = 0,
        var emergencyContactName: String = "",
        var emergencyContactNumber: String = ""
)