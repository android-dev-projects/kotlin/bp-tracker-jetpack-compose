package com.monady.bptrackerjetpackcompose.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.text.DateFormat

@Entity(tableName = "bp_readings_table")
data class BpReading(
        @PrimaryKey(autoGenerate = true)
        var id: Int = 0,
        var created_at: Long = 0,
        var updated_at: Long = 0,
        var high: Int = 0,
        var low: Int = 0,
        var indicatorImage: Int = 0
)

fun BpReading.getCreatedAtFormatted(): String{
        return DateFormat.getDateTimeInstance().format(this.created_at)
}
fun BpReading.getUpdatedAtFormatted(): String{
        return DateFormat.getDateTimeInstance().format(this.updated_at)
}