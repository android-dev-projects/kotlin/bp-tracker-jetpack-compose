package com.monady.bptrackerjetpackcompose.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.monady.bptrackerjetpackcompose.ui.theme.BPTrackerJetpackComposeTheme

@Composable
fun AlertDialogContent(
    modifier: Modifier = Modifier,
    onDismissRequest: () -> Unit = {},
    onConfirm: () -> Unit = {},
    onDismiss: () -> Unit = {},
    titleText: String = "Proceed?",
    confirmText: String = "Confirm",
    dismissText: String = "Dismiss",
    dialogText: String = "Are you sure you want to proceed?",
    showConfirmButton: Boolean = true
){
    AlertDialog(
        modifier = modifier,
        onDismissRequest = onDismissRequest,
        confirmButton = {
            if(showConfirmButton)
                Button(
                    onClick = onConfirm,
                    modifier = modifier
                ){
                    Text(
                        text = confirmText,
                        modifier = modifier
                    )
                }
        },
        dismissButton = {
            Button(
                onClick = onDismiss,
                modifier = modifier
            ){
                Text(
                    text = dismissText,
                    modifier = modifier
                )
            }
        },
        title = {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(5.dp),
                modifier = modifier
            ){
                HeartLogo(
                    size = 25,
                    modifier = modifier
                )
                Text(
                    text = titleText,
                    modifier = modifier
                )
            }
        },
        text = {
            Text(
                text = dialogText,
                modifier = modifier
            )
        },
    )
}

@Preview
@Composable
fun AlertDialogContentPreview(){
    BPTrackerJetpackComposeTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colors.background
        ) {
            AlertDialogContent()
        }
    }
}