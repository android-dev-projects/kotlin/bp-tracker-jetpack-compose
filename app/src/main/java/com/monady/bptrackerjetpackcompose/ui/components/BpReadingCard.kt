package com.monady.bptrackerjetpackcompose.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.monady.bptrackerjetpackcompose.models.BpReading
import com.monady.bptrackerjetpackcompose.models.getUpdatedAtFormatted
import com.monady.bptrackerjetpackcompose.ui.theme.BPTrackerJetpackComposeTheme

@Composable
fun BpReadingCard(
    modifier: Modifier = Modifier,
    reading: BpReading = defaultReading,
    onCardClick: () -> Unit = {}
){
    Card(
        modifier = modifier
            .fillMaxWidth()
            .padding(5.dp)
            .clickable { onCardClick() },
        elevation = 15.dp,
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(10.dp)
        ){
            HeartLogo(
                size = 100,
                modifier = modifier
                    .padding(5.dp)
            )
            Column(
                horizontalAlignment = Alignment.Start,
                modifier = modifier.fillMaxWidth()/*.padding(5.dp)*/,
                verticalArrangement = Arrangement.spacedBy(10.dp)
            ){
                Text(
                    text = reading.getUpdatedAtFormatted(),
                    fontSize = 20.sp
                )
                RowTextCouple(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangementDpSpacing = 10.dp,
                    firstField = "High",
                    secondField = "${reading.high}",
                    fontSizeSp = 20.sp
                )
                RowTextCouple(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangementDpSpacing = 15.dp,
                    firstField = "Low",
                    secondField = "${reading.low}",
                    fontSizeSp = 20.sp
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun BpReadingCardPreview(){
    BPTrackerJetpackComposeTheme {
        Surface(
            modifier = Modifier,
            color = MaterialTheme.colors.background
        ) {
            BpReadingCard(reading = defaultReading)
        }
    }
}

//just for the preview/default value until further refinement pass
val defaultReading = BpReading(
    id = 0,
    high = 120,
    low = 80,
    updated_at = 5,
    created_at = 5,
    indicatorImage = 1
)