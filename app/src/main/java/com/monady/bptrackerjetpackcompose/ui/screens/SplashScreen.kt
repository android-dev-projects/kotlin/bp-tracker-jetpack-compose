package com.monady.bptrackerjetpackcompose.ui.screens

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.runtime.*
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.monady.bptrackerjetpackcompose.navigation.AppRoutes
import com.monady.bptrackerjetpackcompose.ui.components.Splash
import kotlinx.coroutines.delay

@Composable
fun SplashScreen(navHostController: NavHostController){
    var startAnimation by remember { mutableStateOf(false) }
    val alphaAnim = animateFloatAsState(
        targetValue = if(startAnimation) 1F else 0F,
        animationSpec = tween(
            durationMillis = 500
        )
    )

    LaunchedEffect(key1 = true){
        startAnimation = true
        delay(500)
        navHostController.navigate(AppRoutes.FirstTimeProfile.name){
            popUpTo(0)
        }
    }

    Splash(alpha = alphaAnim.value)
}

@Preview
@Composable
fun SplashScreenPreview(){
    SplashScreen(navHostController = rememberNavController())
}

@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
fun SplashScreenDarkPreview(){
    SplashScreen(navHostController = rememberNavController())
}