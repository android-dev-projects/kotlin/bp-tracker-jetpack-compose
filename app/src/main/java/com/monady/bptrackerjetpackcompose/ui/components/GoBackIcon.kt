package com.monady.bptrackerjetpackcompose.ui.components

import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp


@Composable
fun GoBackIcon(modifier: Modifier = Modifier, onGoBackClick: () -> Unit = {}){
    IconButton(
        modifier = modifier,
        onClick = {
            onGoBackClick()
        }
    ){
        Icon(
            imageVector = Icons.Sharp.ArrowBack,
            contentDescription = "Go Back",
            modifier = modifier.size(30.dp),
            tint = Color(0xccf44336)
        )
    }
}

@Preview
@Composable
fun GoBackIconPreview(){
    GoBackIcon()
}