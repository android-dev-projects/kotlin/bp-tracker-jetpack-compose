package com.monady.bptrackerjetpackcompose.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun RowTextCouple(
    verticalAlignment: Alignment.Vertical = Alignment.CenterVertically,
    horizontalArrangementDpSpacing: Dp = 10.dp,
    firstField: String = "",
    secondField: String = "",
    fontSizeSp: TextUnit = TextUnit.Unspecified,
    modifier: Modifier = Modifier
){
    Row(
        verticalAlignment = verticalAlignment,
        horizontalArrangement = Arrangement.spacedBy(horizontalArrangementDpSpacing),
        modifier = modifier
    ){
        Text(
            text = firstField,
            fontSize = fontSizeSp,
            modifier = modifier
        )
        Text(
            text = secondField,
            fontSize = fontSizeSp,
            modifier = modifier
        )
    }
}

@Preview
@Composable
fun RowTextCouplePreview(){
    Surface(
    ) {
        RowTextCouple(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangementDpSpacing = 10.dp,
            firstField = "First Field",
            secondField = "Second Field",
            fontSizeSp = 15.sp,
            modifier = Modifier
        )
    }
}