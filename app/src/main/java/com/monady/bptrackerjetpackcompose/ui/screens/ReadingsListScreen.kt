package com.monady.bptrackerjetpackcompose.ui.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.monady.bptrackerjetpackcompose.models.BpReading
import com.monady.bptrackerjetpackcompose.navigation.AppRoutes
import com.monady.bptrackerjetpackcompose.ui.components.AlertDialogContent
import com.monady.bptrackerjetpackcompose.ui.components.BpReadingCard
import com.monady.bptrackerjetpackcompose.ui.components.EmptyList
import com.monady.bptrackerjetpackcompose.ui.components.MoreOptionsIcon
import com.monady.bptrackerjetpackcompose.viewmodel.BpViewModel
import kotlinx.coroutines.launch

@Composable
fun ReadingsListScreen(
    navHostController: NavHostController,
    viewModel: BpViewModel,
){
    val readingsList by viewModel.bpReadingsList.observeAsState()
    val scope = rememberCoroutineScope()

    ReadingsListContent(
        readingsList = readingsList,
        onAddNewReading = {
            navHostController.navigate(AppRoutes.NewReading.toString())
        },
        onReadingClicked = {
            scope.launch {
                viewModel.getReadingById(it)
            }
            navHostController.navigate(AppRoutes.ReadingDetails.toString())
        },
        onDeleteAllClicked = {
            scope.launch {
                viewModel.deleteAllReadings()
            }
        }
    )
}

@Composable
fun ReadingsListContent(
    modifier: Modifier = Modifier,
    readingsList: List<BpReading>? = listOf(),
    onAddNewReading: () -> Unit = {},
    onReadingClicked: (Int) -> Unit = {},
    onDeleteAllClicked: () -> Unit = {}
){
    Scaffold(
        floatingActionButton = @Composable{
            FloatingActionButton(
                onClick = onAddNewReading,
            ){
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = "Add new record"
                )
            }
        }
    ) {
        var showOptionsMenu by rememberSaveable { mutableStateOf(false) }

        var enableDelete by rememberSaveable{ mutableStateOf(false) }

        val toggleOptionsMenu = {
            showOptionsMenu = !showOptionsMenu
        }

        val toggleDelete = {
            enableDelete = !enableDelete
        }

        if(!readingsList.isNullOrEmpty()){

            Column{
                MoreOptionsIcon(
                    paddingTop = 10,
                    paddingBottom = 5,
                    paddingEnd = 5,
                    onIconClick = toggleOptionsMenu
                )

                LazyColumn(
                    verticalArrangement = Arrangement.spacedBy(5.dp),
                    contentPadding = it,
                    state = rememberLazyListState(),
                    modifier = modifier.fillMaxSize()
                ){
                    items(readingsList){ reading ->
                        BpReadingCard(
                            reading = reading,
                            onCardClick = {
                                onReadingClicked(reading.id)
                            }
                        )
                    }
                }
            }
        }
        else{
            EmptyList(modifier = modifier)
        }

        if(showOptionsMenu){
            TopAppBar(
                backgroundColor = Color.White,
                title = {  },
                elevation = 0.dp,
                actions = {
                    DropdownMenu(
                        expanded = showOptionsMenu,
                        onDismissRequest = toggleOptionsMenu
                    ) {
                        DropdownMenuItem(
                            onClick = {
                                toggleDelete()
                                toggleOptionsMenu()
                            }
                        ) {
                            Text(
                                text = "Delete All Readings"
                            )
                        }
                    }
                }
            )
        }

        if(enableDelete){
            AlertDialogContent(
                onDismissRequest = toggleDelete,
                onDismiss = toggleDelete,
                onConfirm = {
                    toggleDelete()
                    onDeleteAllClicked()
                },
                titleText = "Delete ALL readings?",
                dialogText = "Are you sure you want to delete all readings? This action is NOT reversible!"
            )
        }
    }
}

@Preview
@Composable
fun ReadingsListScreenPreview(){
    Surface(
        modifier = Modifier.fillMaxHeight(),
    ) {
        ReadingsListContent()
    }
}
