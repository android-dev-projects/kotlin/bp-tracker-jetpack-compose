package com.monady.bptrackerjetpackcompose.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.monady.bptrackerjetpackcompose.ui.components.GoBackIcon
import com.monady.bptrackerjetpackcompose.ui.components.HeartLogo
import com.monady.bptrackerjetpackcompose.ui.components.InputTextFieldWrapper
import com.monady.bptrackerjetpackcompose.ui.theme.BPTrackerJetpackComposeTheme
import com.monady.bptrackerjetpackcompose.viewmodel.BpViewModel
import kotlinx.coroutines.launch

@Composable
fun NewReadingScreen(
    focusManager: FocusManager,
    onGoBackClicked: () -> Unit = {},
    viewModel: BpViewModel
){
    val scope = rememberCoroutineScope()

    NewReadingContent(
        focusManager = focusManager,
        onGoBackClicked = onGoBackClicked,
        onAddNewReadingClicked = { high, low ->
            scope.launch{
                viewModel.addReading(high, low)
                onGoBackClicked()
            }
        }
    )
}

@Composable
fun NewReadingContent(
    modifier : Modifier = Modifier,
    focusManager: FocusManager,
    onGoBackClicked: () -> Unit = {},
    onAddNewReadingClicked: (Int, Int) -> Unit = {  high: Int, low: Int -> }
){
    var newHighReading by rememberSaveable{ mutableStateOf("") }
    var newLowReading by rememberSaveable{ mutableStateOf("") }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,
        modifier = modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .padding(vertical = 16.dp)
    ){

        GoBackIcon(
            modifier = modifier.align(Alignment.Start),
            onGoBackClick = onGoBackClicked
        )
        HeartLogo()
        InputTextFieldWrapper(
            modifier = modifier,
            label = "High Reading",
            inputType = KeyboardType.Number,
            nextAction = ImeAction.Next,
            onTextChange = {
                newHighReading = it
            }
        )
        Spacer(
            modifier = Modifier.height(50.dp)
        )
        InputTextFieldWrapper(
            modifier = modifier,
            label = "Low Reading",
            inputType = KeyboardType.Number,
            nextAction = ImeAction.Done,
            focusManager = focusManager,
            onTextChange = {
                newLowReading = it
            }
        )
        Spacer(
            modifier = Modifier.height(50.dp)
        )
        Button(
            modifier = modifier
                .align(Alignment.End)
                .padding(4.dp),
            onClick =  {
                if(newHighReading.isNotEmpty() && newLowReading.isNotEmpty())
                    onAddNewReadingClicked(newHighReading.trim().toInt(), newLowReading.trim().toInt())
            }
        ){
            Text(
                text = "Add new reading",
                modifier = modifier
            )
        }

    }
}

@Preview
@Composable
fun NewReadingScreenPreview(){
    BPTrackerJetpackComposeTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colors.background
        ) {
            NewReadingContent(focusManager = LocalFocusManager.current)
        }
    }
}