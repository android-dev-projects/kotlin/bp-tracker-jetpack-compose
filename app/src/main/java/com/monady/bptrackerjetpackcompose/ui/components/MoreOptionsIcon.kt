package com.monady.bptrackerjetpackcompose.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.Surface
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.MoreVert
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun MoreOptionsIcon(
    modifier: Modifier = Modifier,
    size: Int = 40,
    paddingTop: Int = 0,
    paddingBottom: Int = 0,
    paddingEnd: Int = 0,
    paddingStart: Int = 0,
    onIconClick: () -> Unit = {}
) {
    Row(
        horizontalArrangement = Arrangement.End,
        modifier = modifier.fillMaxWidth()
    ){
        Icon(
            imageVector = Icons.Sharp.MoreVert,
            contentDescription = "More options",
            modifier = modifier
                .size(size.dp)
                .padding(top = paddingTop.dp, start = paddingStart.dp, bottom = paddingBottom.dp, end = paddingEnd.dp )
                .clickable {
                 onIconClick()
                }
        )
    }
}

@Preview(showBackground = true)
@Composable
fun MoreOptionsIconPreview(){
    Surface(
        modifier = Modifier.fillMaxHeight(),
    ) {
        MoreOptionsIcon()
    }
}