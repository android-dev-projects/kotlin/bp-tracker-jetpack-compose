package com.monady.bptrackerjetpackcompose.ui.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.monady.bptrackerjetpackcompose.models.UserProfile
import com.monady.bptrackerjetpackcompose.ui.components.AlertDialogContent
import com.monady.bptrackerjetpackcompose.ui.components.DropDownFieldWrapper
import com.monady.bptrackerjetpackcompose.ui.components.GoBackIcon
import com.monady.bptrackerjetpackcompose.ui.components.InputTextFieldWrapper
import com.monady.bptrackerjetpackcompose.ui.theme.BPTrackerJetpackComposeTheme
import com.monady.bptrackerjetpackcompose.viewmodel.UserProfileViewModel
import kotlinx.coroutines.launch

@Composable
fun FirstTimeUserProfileScreen(
    modifier: Modifier = Modifier,
    bloodTypesList: List<String> = listOf("A+", "A-", "B+", "B-", "O+", "O-", "AB+", "AB-"),
    focusManager: FocusManager = LocalFocusManager.current,
    viewModel: UserProfileViewModel,
    onSuccssfulUserCreation: () -> Unit = {},
    onExitCallback: () -> Unit = {}
){
    val scope = rememberCoroutineScope()

    var showSuccessDialog by rememberSaveable { mutableStateOf(false) }

    val toggleSuccessDialog = {
        showSuccessDialog = !showSuccessDialog
    }

    var showWelcomeDialog by rememberSaveable { mutableStateOf(true) }

    val toggleWelcomeDialog = {
        showWelcomeDialog = !showWelcomeDialog
    }

    FirstTimeUserProfileContent(
        modifier = modifier,
        bloodTypesList = bloodTypesList,
        focusManager = focusManager,
        onExitCallback = {
            toggleWelcomeDialog()
        },
        onSaveUserProfile = { fn, ln, bt, hr, lr, en, ec ->
            val userProfile = UserProfile(
                firstName = fn,
                lastName = ln,
                bloodType = bt,
                normalHighReading = hr,
                normalLowReading = lr,
                emergencyContactName = ec,
                emergencyContactNumber = en
            )

            scope.launch {
                viewModel.addUserProfile(userProfile)
                toggleSuccessDialog()
            }
        }
    )
    if(showSuccessDialog)
        AlertDialogContent(
            onDismissRequest = {
                onSuccssfulUserCreation()
                toggleSuccessDialog()
            },
            onDismiss = {
                onSuccssfulUserCreation()
                toggleSuccessDialog()
            },
            dismissText = "Continue",
            showConfirmButton = false,
            titleText = "Congratulations!",
            dialogText = "User profile created successfully."
        )

    if(showWelcomeDialog){
        AlertDialogContent(
            onDismissRequest = onExitCallback,
            onDismiss = onExitCallback,
            onConfirm = toggleWelcomeDialog,
            titleText = "Welcome to BP Tracker!",
            dialogText = "Please fill the following fields accurately!\nNote: You can not start using the app WITHOUT filling this form!",
            confirmText = "Proceed",
            dismissText = "Exit"
        )
    }
}

@Composable
fun FirstTimeUserProfileContent(
    modifier: Modifier = Modifier,
    bloodTypesList: List<String> = listOf(),
    focusManager: FocusManager,
    onExitCallback: () -> Unit = {},
    onSaveUserProfile: (firstName: String,
                        lastName: String,
                        bloodType: String,
                        normalHighReading: Int,
                        normalLowReading: Int,
                        emergencyName: String,
                        emergencyNumber: String) -> Unit = {fn: String, ls: String, bt: String, hr: Int, lr: Int, en: String, ec: String ->}
){
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,
        modifier = modifier
            .verticalScroll(rememberScrollState())
            .padding(vertical = 16.dp)
    ){
        var firstName by rememberSaveable { mutableStateOf("") }
        var lastName by rememberSaveable { mutableStateOf("") }
        var bloodType by rememberSaveable { mutableStateOf("") }
        var normalHigh by rememberSaveable { mutableStateOf("") }
        var normalLow by rememberSaveable { mutableStateOf("") }
        var contactName by rememberSaveable { mutableStateOf("") }
        var contactNumber by rememberSaveable { mutableStateOf("") }

        var firstNameError by rememberSaveable { mutableStateOf(false) }
        var lastNameError by rememberSaveable { mutableStateOf(false) }
        var bloodTypeError by rememberSaveable { mutableStateOf(false) }
        var normalHighError by rememberSaveable { mutableStateOf(false) }
        var normalLowError by rememberSaveable { mutableStateOf(false) }
        var contactNameError by rememberSaveable { mutableStateOf(false) }
        var contactNumberError by rememberSaveable { mutableStateOf(false) }

        var showErrorDialog by rememberSaveable { mutableStateOf(false) }

        val toggleErrorDialog = {
            showErrorDialog = !showErrorDialog
        }

        GoBackIcon(
            modifier = modifier.align(Alignment.Start),
            onGoBackClick = onExitCallback
        )
        InputTextFieldWrapper(
            label = "First Name",
            nextAction = ImeAction.Next,
            isError = firstNameError,
            onFieldError = {
                firstNameError = it
            },
            onTextChange = {
                firstName = it
            }
        )
        InputTextFieldWrapper(
            label = "Last Name",
            nextAction = ImeAction.Next,
            isError = lastNameError,
            onFieldError = {
                lastNameError = it
            },
            onTextChange = {
                lastName = it
            }
        )
        DropDownFieldWrapper(
            label = "Blood Type",
            nextAction = ImeAction.Next,
            listItems = bloodTypesList,
            isError = bloodTypeError,
            onFieldError = {
                bloodTypeError = it
            },
            onListItemClicked = {
                bloodType = it
            }

        )
        InputTextFieldWrapper(
            label = "Normal High Reading",
            inputType = KeyboardType.Number,
            nextAction = ImeAction.Next,
            isError = normalHighError,
            onFieldError = {
                normalHighError = it
            },
            onTextChange = {
                normalHigh = it
            }
        )
        InputTextFieldWrapper(
            label = "Normal Low Reading",
            inputType = KeyboardType.Number,
            nextAction = ImeAction.Next,
            isError = normalLowError,
            onFieldError = {
                normalLowError = it
            },
            onTextChange = {
                normalLow = it
            }
        )
        InputTextFieldWrapper(
            label = "Emergency Contact Name",
            nextAction = ImeAction.Next,
            isError = contactNameError,
            onFieldError = {
                contactNameError = it
            },
            onTextChange = {
                contactName = it
            }
        )
        InputTextFieldWrapper(
            label = "Emergency Contact Number",
            inputType = KeyboardType.Number,
            nextAction = ImeAction.Done,
            focusManager = focusManager,
            isError = contactNumberError,
            onFieldError = {
                contactNameError = it
            },
            onTextChange = {
                contactNumber = it
            }
        )
        Button(
            modifier = modifier
                .align(Alignment.End)
                .padding(5.dp),
            onClick = {
                //onSaveUserProfile()
                      if(firstName.isNotEmpty() && lastName.isNotEmpty() && bloodType.isNotEmpty()
                          && normalHigh.isNotEmpty() && normalLow.isNotEmpty() && contactName.isNotEmpty() && contactNumber.isNotEmpty()){
                          if( !firstNameError && !lastNameError && !bloodTypeError && !normalHighError && !normalLowError && !contactNameError && !contactNumberError )
                              onSaveUserProfile(
                                  firstName.trim(),
                                  lastName.trim(),
                                  bloodType.trim(),
                                  normalHigh.trim().toInt(),
                                  normalLow.trim().toInt(),
                                  contactName.trim(),
                                  contactNumber.trim()
                              )
                      }
                      else
                          toggleErrorDialog()
            },
            colors = ButtonDefaults.outlinedButtonColors(
                backgroundColor = Color(0xccf44336),
                contentColor = Color.White
            )){
            Text("Save User Profile")
        }
        if(showErrorDialog){
            AlertDialogContent(
                onDismissRequest = toggleErrorDialog,
                onDismiss = toggleErrorDialog,
                dismissText = "Return",
                showConfirmButton = false,
                titleText = "One or more fields are not complete!",
                dialogText = "Make sure to fill all the required fields to proceed."
            )
        }
    }
}

@Composable
@Preview
fun FirstTimePreview(){
    BPTrackerJetpackComposeTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colors.background
        ) {
            FirstTimeUserProfileContent(
                focusManager = LocalFocusManager.current,
            )
        }
    }
}