package com.monady.bptrackerjetpackcompose.ui.components

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun ErrorText(
    modifier: Modifier = Modifier,
    onErrorText: String = "Error"
){
    Text(
        modifier = modifier./*align(Alignment.Start).*/padding(10.dp),
        text = onErrorText,
        color = Color(0xccf44336)
    )
}

@Preview
@Composable
fun ErrorTextPreview(){
    Surface(){
        ErrorText()
    }
}