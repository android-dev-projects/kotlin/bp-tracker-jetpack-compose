package com.monady.bptrackerjetpackcompose.ui.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.Clear
import androidx.compose.material.icons.sharp.Info
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp


@Composable
fun InputTextFieldWrapper(
    textValue: String = "",
    modifier: Modifier = Modifier,
    label: String = "",
    inputType: KeyboardType = KeyboardType.Text,
    nextAction: ImeAction = ImeAction.Default,
    focusManager: FocusManager = LocalFocusManager.current,
    readOnly: Boolean = false,
    isError: Boolean = false,
    onTextChange: (String) -> Unit = {},
    onErrorText: String = "Field must NOT be empty!",
    onFieldError: (Boolean) -> Unit = {}
){
    var isError by rememberSaveable { mutableStateOf(isError) }

    Column{
        InputTextField(
            textValue = textValue,
            modifier = modifier,
            label = label,
            inputType = inputType,
            nextAction = nextAction,
            focusManager = focusManager,
            readOnly = readOnly,
            onError = {
                isError = it
                onFieldError(it)
            },
            isError = isError,
            onTextChange = onTextChange
        )
        if(isError)
            ErrorText(
                modifier = modifier,
                onErrorText = onErrorText
            )
    }
}

@Composable
private fun InputTextField(
    textValue: String = "",
    modifier: Modifier = Modifier,
    label: String = "",
    inputType: KeyboardType = KeyboardType.Text,
    nextAction: ImeAction = ImeAction.Default,
    focusManager: FocusManager = LocalFocusManager.current,
    readOnly: Boolean = false,
    isError: Boolean = false,
    onError: (Boolean) -> Unit = {},
    onTextChange: (String) -> Unit = {}
){
    var textValue by rememberSaveable { mutableStateOf(textValue) }
    var isError by rememberSaveable { mutableStateOf(isError) }

    OutlinedTextField(
        placeholder = { Text("Enter $label") },
        keyboardOptions = KeyboardOptions(keyboardType = inputType, imeAction = nextAction),
        keyboardActions = KeyboardActions(
            onNext = {
                isError = textValue.isEmpty()
                onError(isError)
                focusManager.moveFocus(
                    focusDirection = FocusDirection.Down
                )
            },
            onDone = {
                isError = textValue.isEmpty()
                onError(isError)
                focusManager.clearFocus()
            }
        ),
        isError = isError,
        readOnly = readOnly,
        value = textValue,
        onValueChange = {
            onTextChange(it)
            textValue = it
            isError = it.isEmpty()
            onError(isError)
        },
        maxLines = 1,
        modifier = modifier
            .padding(5.dp)
            .fillMaxWidth(),
        label = { Text(label) },
        colors = TextFieldDefaults.outlinedTextFieldColors(
            backgroundColor = Color.White,
            focusedBorderColor = if(!readOnly) Color(0xccf44336) else Color.Gray,
            unfocusedBorderColor = Color.Gray,//Color(0xccf44336),
            focusedLabelColor = Color(0xccf44336),
            unfocusedLabelColor = Color.Gray,
            textColor = Color(0xccf44336),
            //errorBorderColor = Color(0xccf44336),
            errorTrailingIconColor = Color(0xccf44336),
            //errorLabelColor = Color(0xccf44336)
        ),
        trailingIcon = @Composable {
            if(textValue.isNotEmpty() && !readOnly)
                IconButton(
                    onClick =  { textValue = "" },
                ){
                    Icon(
                        imageVector = Icons.Sharp.Clear,
                        contentDescription = "Clear field",
                    )
                }
            if(isError && !readOnly)
                IconButton(
                    onClick = {}
                ){
                    Icon(
                        imageVector = Icons.Sharp.Info,
                        contentDescription = "Error"
                    )
                }
        }
    )
}

@Preview
@Composable
fun InputTextFieldPreview(){
    InputTextFieldWrapper(
        label = "First Name",
        isError = true,
        readOnly = false
    )
}