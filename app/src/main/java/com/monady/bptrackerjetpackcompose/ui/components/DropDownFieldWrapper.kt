package com.monady.bptrackerjetpackcompose.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.ArrowDropDown
import androidx.compose.material.icons.sharp.Info
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.focus.*
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun DropDownFieldWrapper(
    modifier: Modifier = Modifier,
    label: String = "",
    inputType: KeyboardType = KeyboardType.Text,
    nextAction: ImeAction = ImeAction.Default,
    focusManager: FocusManager = LocalFocusManager.current,
    readOnly: Boolean = true,
    isError: Boolean = false,
    listItems : List<String> = listOf(),
    onListItemClicked: (String) -> Unit = {},
    onErrorText: String = "Field must NOT be empty!",
    onFieldError: (Boolean) -> Unit = {}
){
    var isError by rememberSaveable { mutableStateOf(isError) }

    Column(){
        DropDownField(
            modifier = modifier,
            label = label,
            inputType = inputType,
            nextAction = nextAction,
            focusManager = focusManager,
            readOnly = readOnly,
            isError = isError,
            onError = {
                isError = it
                onFieldError(it)
            },
            listItems = listItems,
            onListItemClicked = onListItemClicked
        )
        if(isError){
            ErrorText(
                modifier = modifier,
                onErrorText = onErrorText
            )
        }
    }
}

@Composable
private fun DropDownField(
    modifier: Modifier = Modifier,
    label: String = "",
    inputType: KeyboardType = KeyboardType.Text,
    nextAction: ImeAction = ImeAction.Default,
    focusManager: FocusManager = LocalFocusManager.current,
    readOnly: Boolean = true,
    isError: Boolean = false,
    onError: (Boolean) -> Unit = {},
    listItems : List<String> = listOf(),
    onListItemClicked: (String) -> Unit = {}
){
    val focusRequester = remember { FocusRequester() }
    var textValue by remember { mutableStateOf("") }
    var isError by rememberSaveable { mutableStateOf(isError) }

    val interactionSource = remember { MutableInteractionSource() }
    val isPressed by interactionSource.collectIsPressedAsState()

    var expanded by rememberSaveable { mutableStateOf(false)}

    val toggleExpansion = {
        expanded = !expanded
    }
    OutlinedTextField(
        enabled = expanded,
        placeholder = { Text("Select $label") },
        keyboardOptions = KeyboardOptions(keyboardType = inputType, imeAction = nextAction),
        keyboardActions = KeyboardActions(
            onNext = {
                isError = textValue.isEmpty()
                onError(isError)
                focusManager.moveFocus(
                    focusDirection = FocusDirection.Down
                )
            },
            onDone = {
                isError = textValue.isEmpty()
                onError(isError)
                focusManager.clearFocus()
            },
        ),
        readOnly = readOnly,
        isError = isError,
        value = textValue,
        onValueChange = {
            onListItemClicked(it)
            textValue = it
            isError = it.isEmpty()
            onError(isError)
        },
        maxLines = 1,
        modifier = modifier
            .padding(5.dp)
            .fillMaxWidth()
            .clickable {
                toggleExpansion()
                focusRequester.requestFocus()
            }
            .focusRequester(focusRequester)
            .onFocusChanged { onError(isError) },
        interactionSource = interactionSource,
        label = { Text(label) },
        colors = TextFieldDefaults.outlinedTextFieldColors(
            backgroundColor = Color.White,
            focusedBorderColor = Color(0xccf44336),
            unfocusedBorderColor = Color(0xccf44336),
            focusedLabelColor = Color(0xccf44336),
            unfocusedLabelColor = Color(0xccf44336),
            textColor = Color(0xccf44336),
            disabledBorderColor = if(!expanded && !isError) Color.Gray else Color(0xccf44336),
            disabledLabelColor = if(!expanded && !isError) Color.Gray else Color(0xccf44336),
            disabledTextColor =  if(!(!expanded && !isError)) Color.Gray else Color(0xccf44336),
            disabledTrailingIconColor = if(!expanded && !isError) Color.Gray else Color(0xccf44336),
            errorBorderColor = Color(0xccf44336),
            errorTrailingIconColor = Color(0xccf44336),
            errorLabelColor = Color(0xccf44336),

        ),
        trailingIcon = @Composable {
            if(isError)
                IconButton(
                    onClick = {}
                ){
                    Icon(
                        imageVector = Icons.Sharp.Info,
                        contentDescription = "Error"
                    )
                }
            else
                IconButton(
                    onClick =  {
                        toggleExpansion()
                        focusRequester.requestFocus()
                        if(!expanded && !isError) {
                            isError = textValue.isEmpty()
                            onError(isError)
                        }
                    },
                ){
                    Icon(
                        modifier = modifier.scale(scaleX = 1f, scaleY = if(expanded) -1f else 1f),
                        imageVector = Icons.Sharp.ArrowDropDown,
                        contentDescription = "Expand field",
                    )
                }
        }
    )
    if(isPressed && expanded) toggleExpansion()
    if(isPressed && textValue.isEmpty()) isError = textValue.isEmpty()
    if(expanded){
        LazyColumn(
            modifier = modifier
                .padding(start = 8.dp, end = 8.dp)
                .shadow(elevation = 1.dp)
                .height(200.dp)
        ){
            items(listItems){ listItemName ->
                Text(
                    text = listItemName,
                    modifier = modifier
                        .clickable {
                            textValue = listItemName
                            isError = false
                            toggleExpansion()
                            onListItemClicked(listItemName)
                        }
                        .fillMaxWidth()
                        .padding(5.dp),
                    fontSize = 20.sp,
                )
            }
        }
    }
}

@Preview
@Composable
fun DropDownFieldPreview(){
    val bloodTypes = listOf("A+", "A-", "B+", "B-", "O+", "O-", "AB+", "AB-")
    DropDownField(
        label = "Blood Type",
        listItems = bloodTypes
    )
}