package com.monady.bptrackerjetpackcompose.ui.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun EmptyList(modifier: Modifier = Modifier){
    Column(
        modifier = modifier.padding(top = 5.dp)
    ) {
        Text(
            text = "There are no entries yet, click the plus (+) button to Add New entries!",
            textAlign = TextAlign.Center,
            fontSize = 40.sp,
            )
        Icon(
            imageVector = Icons.Default.ArrowBack,
            modifier = modifier.rotate(225f).fillMaxHeight().fillMaxWidth(),
            contentDescription = null,
            tint = Color(0xccf44336)
        )
    }
}

@Preview
@Composable
fun EmptyListPreview(){
    EmptyList()
}