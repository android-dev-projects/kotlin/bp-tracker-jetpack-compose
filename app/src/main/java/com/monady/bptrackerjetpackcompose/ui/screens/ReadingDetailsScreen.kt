package com.monady.bptrackerjetpackcompose.ui.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.monady.bptrackerjetpackcompose.models.BpReading
import com.monady.bptrackerjetpackcompose.models.getCreatedAtFormatted
import com.monady.bptrackerjetpackcompose.models.getUpdatedAtFormatted
import com.monady.bptrackerjetpackcompose.ui.components.*
import com.monady.bptrackerjetpackcompose.ui.theme.BPTrackerJetpackComposeTheme
import com.monady.bptrackerjetpackcompose.viewmodel.BpViewModel
import kotlinx.coroutines.launch

@Composable
fun ReadingDetailsScreen(
    focusManager: FocusManager,
    viewModel: BpViewModel,
    onGoBackClick: () -> Unit,
){
    val bpReading by viewModel.reading.observeAsState()

    val scope = rememberCoroutineScope()

    bpReading?.let{
        ReadingDetailsContent(
            focusManager = focusManager,
            onGoBackClick = onGoBackClick,
            bpReading = it,
            onHighChange = {
                if(it.isNotEmpty())
                    (viewModel::updateHighReading)(it.trim().toInt())
            },
            onLowChange = {
                if(it.isNotEmpty())
                    (viewModel::updateLowReading)(it.trim().toInt())
            },
            onSaveReadingClick = { bpReading ->
                scope.launch {
                    viewModel.updateReading(reading = bpReading)
                    onGoBackClick()
                }
            },
            onDeleteReadingClick = { readingId ->
                scope.launch {
                    //Need to show Dialog before delete
                    viewModel.deleteReadingById(readingId = readingId)
                    onGoBackClick()
                }
            }
        )
    }

}

@Composable
fun ReadingDetailsContent(
    modifier: Modifier = Modifier,
    focusManager: FocusManager,
    onGoBackClick: () -> Unit = {},
    onSaveReadingClick: (BpReading) -> Unit = {},
    onDeleteReadingClick: (Int) -> Unit = {},
    bpReading: BpReading,
    onHighChange: (String) -> Unit = {},
    onLowChange: (String) -> Unit = {}
){
    var enableEdit by rememberSaveable { mutableStateOf(true) }

    var enableDelete by rememberSaveable{ mutableStateOf(false) }

    val toggleEdit = {
        enableEdit = !enableEdit
    }

    val toggleDelete = {
        enableDelete = !enableDelete
    }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,
        modifier = modifier
            .verticalScroll(rememberScrollState())
            .padding(vertical = 16.dp)
    ) {
        GoBackIcon(
            modifier = modifier.align(Alignment.Start),
            onGoBackClick = { onGoBackClick() }
        )
        HeartLogo(
            modifier = modifier.align(Alignment.CenterHorizontally)
        )
        Column(
            modifier = modifier.padding(top = 10.dp),
            //horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(20.dp)
        ){
            RowTextCouple(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangementDpSpacing = 15.dp,
                firstField = "Created at:",
                secondField = bpReading.getCreatedAtFormatted()
            )
            RowTextCouple(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangementDpSpacing = 10.dp,
                firstField = "Updated at:",
                secondField = bpReading.getUpdatedAtFormatted()
            )
        }
        InputTextFieldWrapper(
            readOnly = enableEdit,
            label = "High Reading",
            inputType = KeyboardType.Number,
            nextAction = ImeAction.Next,
            textValue = "${bpReading.high}",
            onTextChange = { onHighChange(it) }
        )
        InputTextFieldWrapper(
            readOnly = enableEdit,
            label = "Low Reading",
            inputType = KeyboardType.Number,
            nextAction = ImeAction.Done,
            focusManager = focusManager,
            textValue = "${bpReading.low}",
            onTextChange = { onLowChange(it) }
        )
        val modifier = modifier
            .align(Alignment.End)
            .padding(end = 16.dp)
        if(enableEdit){
            Button(
                modifier = modifier,
                onClick = toggleEdit
            ){
                Text(
                    text = "Update reading"
                )
            }
        }
        else{
            Button(
                modifier = modifier,
                onClick = {
                    onSaveReadingClick(bpReading)
                    toggleEdit()
                }
            ){
                Text(
                    text = "Save reading"
                )
            }
        }
        Button(
            modifier = modifier,
            onClick = toggleDelete
        ){
            Text(
                text = "Delete reading"
            )
        }
    }

    if(enableDelete){
        AlertDialogContent(
            onDismissRequest = toggleDelete,
            onDismiss = toggleDelete,
            onConfirm = {
                toggleDelete()
                onDeleteReadingClick(bpReading.id)
            },
            titleText = "Delete ALL readings?",
            dialogText = "Are you sure you want to delete all readings? This action is NOT reversible!"
        )
    }
}

@Preview
@Composable
fun ReadingDetailsPreview(){
    BPTrackerJetpackComposeTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colors.background
        ) {
            ReadingDetailsContent(
                focusManager = LocalFocusManager.current,
                bpReading = BpReading(0,0,0,120,80,0)
            )
        }
    }
}