package com.monady.bptrackerjetpackcompose.ui.components

import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.monady.bptrackerjetpackcompose.R

@Composable
fun HeartLogo(modifier: Modifier = Modifier, size: Int = 256){
    Icon(
        modifier = modifier
            .size(size.dp),
        painter = painterResource(id = R.drawable.ic_heart_logo),
        contentDescription = "heart logo",
        tint = Color(0xccf44336)
    )
}


@Preview
@Composable
fun HeartLogoPreview(){
    HeartLogo()
}