package com.monady.bptrackerjetpackcompose.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun Splash(modifier: Modifier = Modifier, alpha: Float){
    Box(
        modifier = modifier
            .background(
                if (isSystemInDarkTheme()) Color.Black else Color.White
            )
            .fillMaxSize(),
        contentAlignment = Alignment.Center
    ){
        HeartLogo(
            modifier = modifier.alpha(alpha)
        )
    }
}

@Preview
@Composable
fun SplashPreview(){
    Splash(alpha = 1F)
}