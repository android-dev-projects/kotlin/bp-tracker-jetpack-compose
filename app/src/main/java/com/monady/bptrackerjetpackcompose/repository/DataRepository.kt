package com.monady.bptrackerjetpackcompose.repository

import com.monady.bptrackerjetpackcompose.database.dao.BpReadingDao
import com.monady.bptrackerjetpackcompose.database.dao.UserProfileDao
import com.monady.bptrackerjetpackcompose.models.BpReading
import com.monady.bptrackerjetpackcompose.models.UserProfile
import dagger.Lazy
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DataRepository @Inject constructor(bpReadingDataDao: Lazy<BpReadingDao>, userProfileDataDao: Lazy<UserProfileDao>){

    private val bpReadingDao: BpReadingDao = bpReadingDataDao.get()
    private val userProfileDao: UserProfileDao = userProfileDataDao.get()

    fun getAllReadings() = bpReadingDao.getAllReadings()

    suspend fun getReadingById(readingId: Int): BpReading{
        return bpReadingDao.getBpReading(readingId)
    }

    suspend fun insertReading(reading: BpReading){
        withContext(Dispatchers.IO){
            bpReadingDao.insertBpReading(reading)
        }
    }

    suspend fun deleteReadingById(readingId: Int){
        withContext(Dispatchers.IO){
            bpReadingDao.deleteBpReading(readingId)
        }
    }

    suspend fun updateReading(reading: BpReading){
        withContext(Dispatchers.IO){
            bpReadingDao.updateBpReading(reading)
        }
    }

    suspend fun deleteAllReadings(){
        withContext(Dispatchers.IO){
            bpReadingDao.deleteAllReadings()
        }
    }

    fun getUserProfile() = userProfileDao.getUserProfile()

    suspend fun insertUserProfile(userProfile: UserProfile){
        withContext(Dispatchers.IO){
            userProfileDao.insertUserProfile(userProfile)
        }
    }

    suspend fun updateUserProfile(userProfile: UserProfile){
        withContext(Dispatchers.IO){
            userProfileDao.updateUserProfile(userProfile)
        }
    }
}