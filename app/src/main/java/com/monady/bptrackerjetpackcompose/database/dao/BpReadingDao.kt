package com.monady.bptrackerjetpackcompose.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.monady.bptrackerjetpackcompose.models.BpReading

@Dao
interface BpReadingDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBpReading(reading: BpReading)

    @Query("select * from bp_readings_table where id = :readingId")
    suspend fun getBpReading(readingId: Int): BpReading

    @Query("select * from bp_readings_table order by updated_at desc")
    fun getAllReadings(): LiveData<List<BpReading>>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateBpReading(reading: BpReading)

    @Query("delete from bp_readings_table where id = :readingId")
    suspend fun deleteBpReading(readingId: Int)

    @Query("delete from bp_readings_table")
    suspend fun deleteAllReadings()
}