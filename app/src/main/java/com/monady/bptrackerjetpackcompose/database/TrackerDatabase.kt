package com.monady.bptrackerjetpackcompose.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.monady.bptrackerjetpackcompose.database.dao.BpReadingDao
import com.monady.bptrackerjetpackcompose.database.dao.UserProfileDao
import com.monady.bptrackerjetpackcompose.models.BpReading
import com.monady.bptrackerjetpackcompose.models.UserProfile

@Database(entities = [BpReading::class, UserProfile::class], version = 1, exportSchema = false)
abstract class TrackerDatabase: RoomDatabase() {
    abstract val bpReadingDao: BpReadingDao
    abstract val userProfileDao: UserProfileDao
}