package com.monady.bptrackerjetpackcompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.platform.LocalFocusManager
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.monady.bptrackerjetpackcompose.navigation.SetNavHost
import com.monady.bptrackerjetpackcompose.ui.theme.BPTrackerJetpackComposeTheme
import com.monady.bptrackerjetpackcompose.utils.InitialPreferenceAppSetup
import com.monady.bptrackerjetpackcompose.utils.NotificationsUtil
import com.monady.bptrackerjetpackcompose.viewmodel.BpViewModel
import com.monady.bptrackerjetpackcompose.viewmodel.UserProfileViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    lateinit var navController: NavHostController
    lateinit var focusManager: FocusManager

    @Inject lateinit var bpViewModel: BpViewModel
    @Inject lateinit var userViewModel: UserProfileViewModel
    @Inject lateinit var initialPreferenceAppSetup: InitialPreferenceAppSetup
    @Inject lateinit var notificationsUtil: NotificationsUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        notificationsUtil.createNotificationChannel()

        initialPreferenceAppSetup.initFirstTimeUse()

        setContent {
            navController = rememberNavController()
            focusManager = LocalFocusManager.current

            BPTrackerJetpackComposeTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    val firstTimeUsage by rememberSaveable { mutableStateOf(initialPreferenceAppSetup.checkFirstTimeApp()) }

                    if(firstTimeUsage) notificationsUtil.buildNotification("Welcome to BPTracker", "Wish you the best!")

                    SetNavHost(
                        checkFirstTimeUse = firstTimeUsage,
                        navHostController = navController,
                        focusManager = focusManager,
                        bpViewModel = bpViewModel,
                        userViewModel = userViewModel,
                        onExitCallback = {
                            this.finish()
                        },
                        setFirstTimeAppEnd = {
                            initialPreferenceAppSetup.setFirstTimeAppEnd()
                        }
                    )
                }
            }
        }
    }
}